# davmail-ansible

A simple role that deploys a davmail container with a specified config and cert to docker running on a debian system (for Exchange to IMAP/SMTP proxy)


## Requirements

- 2 files placed in the `files/` directory at the root(playbook level) of your project
    - davmail.properties - davmail config (ansible vaulting this would be a good idea)
    - davmai.p12 - Certificate bundle (ansible vaulting this would be a good idea)
